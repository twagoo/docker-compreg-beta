#!/bin/bash

ARCHIVE_DATABASE="https://b2drop.eudat.eu/index.php/s/7HOKKlLfIzqf13D/download"
ARCHIVE_WEBAPP="https://github.com/clarin-eric/component-registry-rest/releases/download/ComponentRegistry-2.2.0-beta6/component-registry-rest-2.2.0-beta6-mpi-deploy.tar.gz"
NAME="component-registry-rest-2.2.0-beta6"

init_data () {
    LOCAL=0
    if [ "$1" == "local" ]; then
        LOCAL=1
    fi

#    if [ "${LOCAL}" -eq 0 ]; then
    echo -n "Fetching remote data"

    echo "Downloading external resources"

    echo "\tDatabase dump" && \
    cd postgres && \
    curl -s -S -J -L -O "$ARCHIVE_DATABASE" && \
    tar -xf *.tar.gz && \
    cd ..

    echo "\tComponent Registry release" && \
    cd webapp && \
    curl -s -S -J -L -O "$ARCHIVE_WEBAPP" && \
    tar -xf *.tar.gz && \
    mkdir -p $NAME/war/compreg && \
    cd $NAME/war/compreg && \
    unzip ../component-registry-rest.war > /dev/null && \
    cp ../../../context.xml META-INF/ && \
    cp ../../../shhaa.xml WEB-INF/ && \
    cp WEB-INF/web-shib.xml WEB-INF/web.xml && \
    cd ../../../..

#   else
#   fi
}

cleanup_data () {
    if [ -f "postgres/component_registry_20160714.sql" ]; then
        echo "\tRemoving postgres/component_registry_20160714.sql*"
        rm postgres/component_registry_20160714.sql*
    fi

    echo "\tRemoving webapp/component-registry-rest-*"
    rm -rf webapp/component-registry-rest-*
}